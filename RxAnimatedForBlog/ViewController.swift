//
//  ViewController.swift
//  RxAnimatedForBlog
//
//  Created by Kyryl Nevedrov on 27/09/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxAnimated

class ViewController: UIViewController {
    @IBOutlet weak var fadeLabel: UILabel!
    @IBOutlet weak var flipLabel: UILabel!
    @IBOutlet weak var tickLabel: UILabel!
    @IBOutlet weak var customLabel: UILabel!
    
    @IBOutlet weak var imageViewIsHiddenLabel: UILabel!
    @IBOutlet weak var imageViewAlphaLabel: UILabel!
    
    @IBOutlet weak var fadeImageView: UIImageView!
    @IBOutlet weak var fadeHiddenImageView: UIImageView!
    @IBOutlet weak var fadeAlphaImageView: UIImageView!
    
    @IBOutlet weak var flipImageView: UIImageView!
    @IBOutlet weak var flipHiddenImageView: UIImageView!
    @IBOutlet weak var flipAlphaImageView: UIImageView!
    
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var tickHiddenImageView: UIImageView!
    @IBOutlet weak var tickAlphaImageView: UIImageView!
    
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var customHiddenImageView: UIImageView!
    @IBOutlet weak var customAlphaImageView: UIImageView!
    
    @IBOutlet weak var blockImageView: UIImageView!
    @IBOutlet weak var blockHiddenImageView: UIImageView!
    @IBOutlet weak var blockAlphaImageView: UIImageView!
    
    @IBOutlet weak var leadingChangeConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingEnabledDisabledConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var fadeButton: UIButton!
    @IBOutlet weak var flipButton: UIButton!
    @IBOutlet weak var tickButton: UIButton!
    @IBOutlet weak var customButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private let timer = Observable<Int>.timer(RxTimeInterval.seconds(0), period: RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
    
    private var isActiveTimer: Observable<Bool> {
        return timer
            .scan(true) { acc, _ in
                return !acc
            }
    }
    private var imageTimer: Observable<UIImage?> {
        return timer
            .scan("face1") { _, count in
                return count % 2 == 0 ? "face1" : "face2"
            }
            .map { name in
                return UIImage(named: name)
            }
    }
    private var constraintTimer: Observable<CGFloat> {
        return timer
            .scan(0) { acc, _ in
                return acc == 0 ? 100 : 0
        }
    }

    private var angle: CGFloat = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        //Labels animations
        //fade
        timer
            .map { "Text + fade [\($0)]" }
            .bind(animated: fadeLabel.rx.animated.fade(duration: 0.33).text)
            .disposed(by: disposeBag)
        //flip
        timer
            .map { "Text + flip [\($0)]" }
            .bind(animated: flipLabel.rx.animated.flip(.top, duration: 0.33).text)
            .disposed(by: disposeBag)
        //tick
        timer
            .map { "Text + tick [\($0)]" }
            .bind(animated: tickLabel.rx.animated.tick(duration: 0.33).text)
            .disposed(by: disposeBag)
        //custom scale
        timer
            .map { "Text + custom [\($0)]" }
            .bind(animated: customLabel.rx.animated.scale(duration: 0.33).text)
            .disposed(by: disposeBag)
        
        //Images animations
        //fade
        imageTimer
            .bind(animated: fadeImageView.rx.animated.fade(duration: 1.0).image)
            .disposed(by: disposeBag)
        //flip
        imageTimer
            .bind(animated: flipImageView.rx.animated.flip(.top, duration: 1.0).image)
            .disposed(by: disposeBag)
        //tick
        imageTimer
            .bind(animated: tickImageView.rx.animated.tick(duration: 1.0).image)
            .disposed(by: disposeBag)
        //custom scale
        imageTimer
            .bind(animated: customImageView.rx.animated.scale(duration: 1.0).image)
            .disposed(by: disposeBag)
        //block animation
        imageTimer
            .bind(animated: blockImageView.rx.animated.animation(duration: 0.5, animations: { [weak self] in
                self?.angle += 0.2
                self?.blockImageView.transform = CGAffineTransform(rotationAngle: self?.angle ?? 0)
            }).image )
            .disposed(by: disposeBag)
        
        //fade + hidden
        imageTimer
            .bind(animated: fadeHiddenImageView.rx.animated.fade(duration: 1.0).image)
            .disposed(by: disposeBag)
        //flip + hidden
        imageTimer
            .bind(animated: flipHiddenImageView.rx.animated.flip(.top, duration: 1.0).image)
            .disposed(by: disposeBag)
        //tick + hidden
        imageTimer
            .bind(animated: tickHiddenImageView.rx.animated.tick(duration: 1.0).image)
            .disposed(by: disposeBag)
        //custom scale + hidden
        imageTimer
            .bind(animated: customHiddenImageView.rx.animated.scale(duration: 1.0).image)
            .disposed(by: disposeBag)
        //block + hidden
        imageTimer
            .bind(animated: blockHiddenImageView.rx.animated.animation(duration: 0.5, animations: { [weak self] in
                self?.angle += 0.2
                self?.blockHiddenImageView.transform = CGAffineTransform(rotationAngle: self?.angle ?? 0)
            }).image )
            .disposed(by: disposeBag)
        
        //fade + alpha
        imageTimer
            .bind(animated: fadeAlphaImageView.rx.animated.fade(duration: 1.0).image)
            .disposed(by: disposeBag)
        //flip + alpha
        imageTimer
            .bind(animated: flipAlphaImageView.rx.animated.flip(.top, duration: 1.0).image)
            .disposed(by: disposeBag)
        //tick + alpha
        imageTimer
            .bind(animated: tickAlphaImageView.rx.animated.tick(duration: 1.0).image)
            .disposed(by: disposeBag)
        //custom scale + alpha
        imageTimer
            .bind(animated: customAlphaImageView.rx.animated.scale(duration: 1.0).image)
            .disposed(by: disposeBag)
        //block + alpha
        imageTimer
            .bind(animated: blockAlphaImageView.rx.animated.animation(duration: 0.5, animations: { [weak self] in
                self?.angle += 0.2
                self?.blockAlphaImageView.transform = CGAffineTransform(rotationAngle: self?.angle ?? 0)
            }).image )
            .disposed(by: disposeBag)
        
        // Animate layout constraint
        constraintTimer
            .bind(animated: leadingChangeConstraint.rx.animated.layout(duration: 0.33).constant )
            .disposed(by: disposeBag)
        isActiveTimer
            .bind(animated: leadingEnabledDisabledConstraint.rx.animated.layout(duration: 0.33).isActive )
            .disposed(by: disposeBag)
        
        //Animate buttons
        //fade
        imageTimer
            .bind(animated: fadeButton.rx.animated.fade(duration: 1.0).backgroundImage)
            .disposed(by: disposeBag)
        isActiveTimer
            .bind(animated: fadeButton.rx.animated.fade(duration: 1.0).isEnabled)
            .disposed(by: disposeBag)
        //flip
        imageTimer
            .bind(animated: flipButton.rx.animated.flip(.top, duration: 1.0).backgroundImage)
            .disposed(by: disposeBag)
        isActiveTimer
            .bind(animated: flipButton.rx.animated.flip(.top, duration: 1.0).isEnabled)
            .disposed(by: disposeBag)
        //tick
        imageTimer
            .bind(animated: tickButton.rx.animated.tick(duration: 1.0).backgroundImage)
            .disposed(by: disposeBag)
        isActiveTimer
            .bind(animated: tickButton.rx.animated.tick(duration: 1.0).isEnabled)
            .disposed(by: disposeBag)
        //custom scale
        imageTimer
            .bind(animated: customButton.rx.animated.scale(duration: 1.0).backgroundImage)
            .disposed(by: disposeBag)
        isActiveTimer
            .bind(animated: customButton.rx.animated.scale(duration: 1.0).isEnabled)
            .disposed(by: disposeBag)
        
    }

}

// This is your class `UIView`
extension AnimatedSink where Base: UIView {
    // This is your animation name `rotate 30`
    public func scale(_ scale: CGFloat = 2, duration: TimeInterval) -> AnimatedSink<Base> {
        // use one of the animation types and provide `setup` and `animation` blocks
        let type = AnimationType<Base>(type: RxAnimationType.spring(damping: 1, velocity: 0), duration: duration, setup: { view in
            view.alpha = 0
            view.transform = CGAffineTransform(scaleX: scale, y: scale)
        }, animations: { view in
            view.alpha = 1
            view.transform = CGAffineTransform.identity
        })
        
        //return AnimatedSink
        return AnimatedSink<Base>(base: self.base, type: type)
    }
}

// This is your class `UILabel`
extension AnimatedSink where Base: UILabel {
    // This is your property name `text` and value type `String`
    public var text: Binder<String> {
        let animation = self.type!
        return Binder(self.base) { label, text in
            animation.animate(view: label, binding: {
                label.text = text
            })
        }
    }
}
